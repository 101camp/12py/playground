> 分享作弊条, 用以快速组织演讲/讨论/展示/...

应用示例:[[CST] camp2py 破冰现场 + wow 预约访谈 的 cheetsheet - ofey404](../-/wikis/How2/How2CheatSheet)

## Meta
> 元数据: pre 的时间,地点,时长,目的与要求,备忘...


## Goal
> pre 目标,要讲清楚的问题,想要达到的效果...


## Timeline
> 按时间顺序排列的重点流程,预估用时,以及 tips...


## Draft
> 讲演时参照的稿子,细节,punchlines...


## Reference
> 过程中参考过的重要文章/图书/模块/代码/...

永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


## logging:
> 用倒序日期排列来从旧到新记要关键变化


- 200315 ZQ 修订 提问的智慧外链
- 190724 ZQ 增强标题结构
- 190714 ZQ 部署到 tasks 仓库
- 190704 ZQ init.
