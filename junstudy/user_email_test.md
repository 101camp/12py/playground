# 修改push时的用户名与邮箱

## 背景

git上传时，在gitlab端发现上传人是个人中文真实名，但gitlab端我没有设置过中文名，猜想是电脑端的用户名被上传了。现在不想让真实名上传，故做一些验证猜想，并修改push时的用户名

![](https://junstudy.oss-cn-shanghai.aliyuncs.com/push_cn_name.png)

下图可以看出，用户名、邮箱自动设置为电脑用户名与主机名
![](https://junstudy.oss-cn-shanghai.aliyuncs.com/push_info.png)

## 操作

修改用户名与邮箱，并查看

修改用户名

```
git config --global user.name "junstudy"
```

修改邮箱
```
git config --global user.email "junstudys@163.com"
```

查看用户名
```
git config user.name
```

查看邮箱
```
git config user.email
```

终端执行如图
![](https://junstudy.oss-cn-shanghai.aliyuncs.com/reset_username.png)

修改文件并再次进行推送后发现gitlab用户名已经改变

![](https://junstudy.oss-cn-shanghai.aliyuncs.com/push_again.png)

## logging

200911 init.
